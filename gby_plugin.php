<?php
/*
Plugin Name: Testimonial
Plugin URI: http://www.softwareseni.com
Description: This plugin adds a custom widget.
Version: 1.0
Author: Prisantya
Author URI: http://www.softwareseni.com
License: GPL2
*/
include ("gby_widget_testi.php");


function gby_pluginaktivasi() {
    ob_start();
    gby_install();
}

function gby_install() {
	global $wpdb;
    global $jal_db_version;
    $jal_db_version = '2.0';

	$table_name = $wpdb->prefix . 'usertestimonial';
	
	$charset_collate = $wpdb->get_charset_collate();

  
	    $sql = "CREATE TABLE $table_name (
		    id tinyint(3) NOT NULL AUTO_INCREMENT,
		    name varchar(30) NOT NULL,
		    phone_number varchar(15) NOT NULL,
		    email varchar(25) DEFAULT '' NOT NULL,
            testimonial text NOT NULL,
            blog_id int(2) NOT NULL,
		    PRIMARY KEY  (id)
	    ) $charset_collate;";
   
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	add_option( 'jal_db_version', $jal_db_version );
}

register_activation_hook( __FILE__, 'gby_pluginaktivasi' );

   function gby_testimonial_form() {
 ?>
    <div class="wrap">
    <h2>Your Testimonial</h2>
    </div>
    <form action="<?php esc_url( $_SERVER['REQUEST_URI'] ); ?>" method="post">

    <p> Your Name (required) <br />
    <input type="text" name="testi-name" value="<?php echo ( isset( $_POST["testi-name"] ) ? esc_attr( $_POST["testi-name"] ) : '' ); ?>" size="40" />
    </p>

    <p>Your Email (required) <br />
    <input type="email" name="testi-email" value="<?php echo  ( isset( $_POST["testi-email"] ) ? esc_attr( $_POST["testi-email"] ) : '' ) ?>" size="40" />
    </p>

    <p> Phone Number (required) <br />
    <input type="text" name="testi-phonenumber" value="<?php echo  ( isset( $_POST["testi-phonenumber"] ) ? esc_attr( $_POST["testi-phonenumber"] ) : '' ) ?>" size="40" />
    </p>

    <p> Your Testimonial (required) <br />
    <textarea rows="10" cols="35" name="testi-testimonial"> <?php echo (isset( $_POST["testi-testimonial"] )) ? esc_attr( $_POST["testi-testimonial"] ):''; ?> </textarea>
    </p>

    <p><input type="submit" name="testi-submitted" value="Send"/></p>
    </form>

 <?php
    }

    function gby_testimonial(){
        ob_start();
        gby_save_testimonial();
        gby_testimonial_form();
        return ob_get_clean();
    }

add_shortcode('testimonial_form','gby_testimonial');

    function gby_save_testimonial() {
        
        global $wpdb;
        if (isset($_POST['testi-submitted'])) {
        // sanitize form values
        $name    = sanitize_text_field( $_POST["testi-name"] );
        $email   = sanitize_email( $_POST["testi-email"] );
        $phone = sanitize_text_field( $_POST["testi-phonenumber"] );
        $testimonial = esc_textarea( $_POST["testi-testimonial"] );
        $forminput=array('name'=>$name,'email'=>$email,'phone'=>$phone,'testi'=>$testimonial);
        $blogid = get_current_blog_id();
        //validasi
        if (gby_validasi($forminput)==true) {
            $data=array(
                'name'=>$name,
                'email'=>$email,
                'phone_number'=>$phone,
                'testimonial'=>$testimonial,
                'blog_id' => $blogid
                );
            
            $type= array(
                '%s',
                '%s',
                '%s',
                '%s',
                '%d'
            );
            
            $save=$wpdb->insert($wpdb->base_prefix.'usertestimonial', $data, $type);
     
            if ($save) {
                echo "<font color='blue'>Thank you for your testimonial</font>";
                $_POST=array();
            } else {
              
                echo "<font color='blue'>Please try again</font>";
            }  
        } else {
            echo gby_validasi($forminput);
        }
        
    }
    }

    function gby_validasi($data) {
        $hasil; $status;    
        
    if (!empty($data['name']) &&  !empty($data['email']) &&  !empty($data['phone']) &&  !empty($data['testi']) ) {
       
        if (is_email($data['email'])) {
            $status=true;
        } else {
            $hasil.="Email address is not valid <br />";
            $status=false;
        }

        if(!is_numeric($data['phone'])) {
            $hasil.="Phone number only contains number <br />";
            $status=false;
        } else if (!(strlen($data['phone'])<=14) && !(strlen($data['phone']>=6))){
            $status=false;
            $hasil.="Phone number should between 6 to 14 digits <br />";
        } else {
            $status=true;
        }
    } else 
    {
        $status=false;
        $hasil.="Please fill in all the columns";
    }

        return !$status? $hasil: $status;
    }

    function gby_admin_menu() {
        add_menu_page('Testimonial', 'Testimonial', 'manage_options', 'testimonial_admin', 'gby_list_testimonial', 'dashicons-tickets', 6  );
    } 

add_action('admin_menu', 'gby_admin_menu');

    function gby_list_testimonial(){
        global $wpdb;
       
        $testi = $wpdb->get_results("select * from ".$wpdb->base_prefix."usertestimonial where blog_id=".get_current_blog_id());
        
        ?>
        <br /> <br />
        <h2> Daftar Testimonial </h2>
        <table>
        <tr>
            <td> No </td>
            <td> Nama </td>
            <td> Email </td>
            <td> Phone Number </td>
            <td> Testimonial </td>
            <td> Action </td>
        </tr>
        <?php
        $no=1;
        foreach ($testi as $data) {
        echo "<tr border='5px'>
                <td>".$no." </td>
                <td>".$data->name."</td>
                <td>".$data->email."</td>
                <td>".$data->phone_number."</td>
                <td>".$data->testimonial,"</td>
                <td><a href='".admin_url('admin.php?page=testimonial_admin&delete=aktif&id='.$data->id, 'http')."'>Delete</a></td>
            </tr>";
        $no++;
        }
        echo "</table>";
    }

add_action('init','gby_delete');

    function gby_delete() {
        global $wpdb;
        If (isset($_GET['delete']) && isset($_GET['id'])) {
            $del_id = $_GET["id"];
            $blog_id = get_current_blog_id();
            
		   $wpdb->query("DELETE FROM ".$wpdb->base_prefix."usertestimonial WHERE id=".$del_id." AND blog_id=".$blog_id);
            wp_redirect('admin.php?page=testimonial_admin');
            exit;
        }
    }


register_deactivation_hook( __FILE__, 'gby_uninstall' );

function gby_uninstall() {
    global $wpdb;
     $table_name = $wpdb->base_prefix . 'usertestimonial';
     $sql = "DROP TABLE IF EXISTS $table_name";
     $wpdb->query($sql);
     delete_option("jal_db_version");
}