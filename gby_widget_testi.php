<?php
class MyWidgets extends WP_Widget {
  /**
   * Mengatur Nama Widget
   */
  public function __construct() {
    $widget_ops = array( 
      'classname' => 'class_testimonial_widget', // class pada html
      'description' => 'Show the testimonial', // deskripsi tentang widget
    );
    
    /**
     * parent::__construct($id_widget, $namaWidget, $options);
     */
    parent::__construct( 'id_testimonial_widget', 'Testimonial', $widget_ops );
  }
  /**
   * yang akan ditampilkan di website
   */
  public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );
    global $wpdb;

    $random= $wpdb->get_row("select * from ".$wpdb->base_prefix."usertestimonial where blog_id=".get_current_blog_id()." order by rand() limit 1");

    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) )
    echo $args['before_title'] . $title . $args['after_title'];
     
    // Tampilkan testimonial jika ada data
    if ($random==NULL) {
        echo "There is no testimonial yet";
      } else {
      echo __( $random->testimonial.'<br /> - '.$random->name.' -', 'id_testimonial_widget' );
    }

    echo $args['after_widget'];
  }
  /**
   * akan ditampilkan di halaman admin
   */
  public function form( $instance ) {
    //Menampilkan nilai default Title
    if ( isset( $instance[ 'title' ] ) ) {
      $title = $instance[ 'title' ];
      }
      else {
      $title = __( 'Testimonial', 'id_testimonial_widget' );
      }
      // Form update title
      ?>
      <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
      </p>
      <?php 
  }
  /**
   * perintah simpan dan update
   */
  public function update( $new_instance, $old_instance ) {
    // code saving
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    return $instance;
  }
}
// implementasi widget
function daftar_widget() {
	register_widget( 'MyWidgets' );
}
add_action( 'widgets_init', 'daftar_widget' );